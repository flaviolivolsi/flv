import React, { Component } from 'react';
import Commands from './commands.js';

export default class FetchCommand extends React.Component {
  render() {
    if (Commands.list().indexOf(this.props.value) === -1) {
      return null;
    }
    
    return (
      <p><strong>{Commands.exec(this.props.value, this.props.state)}</strong></p>
    );
  }
}