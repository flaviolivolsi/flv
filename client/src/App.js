import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Commands from './commands.js';
import FetchCommand from './fetchcommand.js';
import Help from './help.js';
import './App.css';

class App extends Component {
  state = {
    commands: [],
    directories: ['/home/flavio/mp3', '/home/flavio/movies', '/home/flavio/thereisnopornhere', '/home/flavio/docs'],
    currentDirectory: '/home/flavio'
  }
  
  changeDir(dir) {
    this.setState({currentDirectory: dir})
  }
  
  handleSubmit(evt) {
    evt.preventDefault();
    
    var commands = this.state.commands;
    commands.push(evt.target[0].value);
    this.setState({commands});
    this.terminalInput.value = '';
    
    var that = this;
    setTimeout(function() {
      ReactDOM.findDOMNode(that.terminalEnd).scrollIntoView({ behavior: 'smooth' });
    }, 50);
  }
  
  handleExternalClick() {
    if (!this.terminalInput) { return; }
    
    this.terminalInput.focus();
  }
  
  componentDidMount() {
    this.terminalInput.focus();
    
    var that = this;
    document.body.addEventListener('click', function() {
      that.handleExternalClick();
    });
  }
  
  render() {    
    return (
      <div className="App">
        <div><Help commands={Commands.list()} /></div>
        {this.state.commands.map(command =>
          <div>
            <p>user@flaviolivolsi:~$ {command}</p>
            <FetchCommand value={command} state={this.state} />
          </div>
        )}
        <form onSubmit={evt => this.handleSubmit(evt)}>
          <div>
            <span>user@flaviolivolsi:~$</span>
            <input
              className="terminal-bar"
              autoComplete="off"
              ref={(input) => { this.terminalInput = input; }}
            />
          </div>
          <div ref={(el) => { this.terminalEnd = el; }}></div>
        </form>
      </div>
    );
  }
}

export default App;
