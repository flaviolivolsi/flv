import React, { Component } from 'react';

export default class Help extends React.Component {
  render() {
    return (
      <div>
        <p>Available commands:</p>
        <ul className="help">
          {this.props.commands.map(command =>
            <li>{command}</li>
          )}
        </ul>
      </div>
    );
  }
}