export default {
  list() {
    return ['help', 'whoami', 'ls'];
  },
  
  exec(command, state) {
    switch(command) {
      case 'whoami': {
        return 'Flavio/Flavinci/Flavioli';
      }
      
      case 'help': {
        return 'no help for you, motherfucker';
      }
      
      case 'ls': {
        var directories = [];
        for (var i=0;i<state.directories.length;i++) {
          var dir = state.directories[i].split('/');
          dir = dir[dir.length-1];
          directories.push(dir);
        }
        
        return directories.join(' ');
      }
    }
  }
}